---
layout: post
lang: es
title: "Categorías y publicaciones"
date: 2021-08-07 17:24:00 +0100
categories: announcements
---

## Categorías
Recientemente, hemos añadido las categorías de «Eventos» y «Anuncios» para las publicaciones a fin de tener una variedad, en contenido, similar a la de otros blogs.

Las categorías actuales, visibles en el menú inicial, están ordenadas por orden de prioridad y, a fin de explicar cuál es la función de cada una, se listan a continuación:

* Noticias
* Recomendaciones
* Tutoriales
* Opiniones
* Eventos
* Anuncios

### Noticias
Se sitúan noticias del mundo del «Software Libre y de Código Abierto».

El contenido puede variar desde las actualizaciones de un software, hasta movimientos decisivos de una organización que contribuya al mismo.

### Recomendaciones
Indicamos «Software Libre y de Código Abierto» que puede resultar interesante o útil para alguna persona.

Además, podemos llegar a dar una pequeña introducción de su uso.

### Tutoriales
Aquí explicamos como realizar determinadas acciones usando «Software Libre y de Código Abierto».

El contenido puede variar desde el objetivo de sustituir a un «Software Privativo» usado para realizar el mismo tipo de acciones, como de indicar un uso avanzado del mismo software que puede ser necesario de forma común o resultar útil.

### Opiniones
Esta categoría tiene una implicación más personal puesto que, cada persona que participa en la redacción puede tener la suya propia. Asimismo, siempre intentaremos mantenernos neutrales en temas agenos a la temática del sitio web, como es el «Software Libre y de Código Abierto».

Los temas a tocar, pueden partir desde la existencia de un software hasta movimientos de alguna entidad que contribuya al mismo.

### Eventos
Es de uso eventual, un poco sugerido por su nombre también pero, la idea es ejecutar ciertos actos que pueden ser entretenidos con la comunidad y que, por obvias razones, no entran en el resto de categorías.

Un ejemplo puede ser la realización de una encuesta de sistemas operativos usados o un concurso de capturas de pantalla.

### Anuncios
Está dedicada a presentar cambios del propio sitio web y del proyecto +FLOSS en sí.

## Publicaciones
Al fin, tras casi 9 meses de espera, nos queda poco para finalizar los preparativos del sitio web y empezar a publicar. Esperamos poder presentarnos en alguna red social inicialmente.

Adicionalmente, tenemos ya algunas publicaciones en reserva, a la espera de revisión, con las que podremos arrancar el blog.

---

**Redacción:** [EchedeyLR](/redaction.html#echedeylr)
