---
layout: post
lang: es
title: "Nuevo inicio"
date: 2021-01-09 13:53:00 -0400
categories: announcements
---

Ofrecemos una cálida bienvenida desde +FLOSS, blog que busca entregar un contenido que se encuentra perdido en la blogosfera hispana del «Software Libre y de Código Abierto». 

Para ello, nos proponemos como meta difundir el «Software Libre y de Código Abierto», enseñar la ética detrás, mostrar sus beneficios e impulsar su uso dentro de la comunidad sin desvirtuarlo difundiendo «Software Privativo».

Si bien de momento el [equipo de redacción](/redaction.html) es el que es, el proyecto está abierto a contribuciones por parte de la comunidad, ya sea mediante ideas en los comentarios o artículos a traves de nuestro [repositorio](https://git.disroot.org/masfloss/web) en la instancia de Gitea de Disroot.

Sin nada más que decir o explicar, que lo disfruten. Dentro de poco debería estar disponible más contenido.

---

**Redacción:** [Germán](/redaction.html#germán)
