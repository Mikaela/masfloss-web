---
layout: post
lang: es
title: "Gitea es ahora propiedad de la compañía Gitea LTD"
date: 2022-10-27 15:31:08 +0100
categories: news
---

Ayer se hacía eco de una noticia en el Fediverso: Gitea ha cambiado de entidad propietaria a una entidad con ánimo de lucro del mismo nombre.

## Un poco de transfondo

Gitea es un una forja de código para el hospedaje de proyectos basados en sistemas de control de versiones, principalmente Git, similar a GitLab.

Nació en 2016 como bifurcación comunitaria de Gogs, otro proyecto del mismo tipo cuando el proyecto cambió bajo el control de una única persona realizando el mantenimiento.

Esto limitó la entrada de retroalimentación y la velocidad con la que la comunidad podía influenciar el proyecto, generando la mencionada bifurcación.

## Situación actual

[Loïc Dachary](https://mastodon.online/@dachary), persona que participa en el proyecto ForgeFriends para proveer a las forjas de código la posibilidad de federar como parte del Fediverso, [dio a conocer la noticia en el Fediverso](https://mastodon.online/@dachary/109235211733571827) sobre el cambio de entidad propietaria tras el [anuncio oficial del proyecto Gitea](https://blog.gitea.io/2022/10/open-source-sustainment-and-the-future-of-gitea/).

Tras ello, la persona encargada de la administración de la cuenta de [Gitea en el Fediverso](https://social.gitea.io/@gitea) respondió que la persona que había formalizado el cambio de propiedad, encargada del proyecto conocida como «Lunnys», había sido siempre la persona propietaria del dominio y la marca.

A esto, Loïc respondió que dicha persona fue escogida por la comunidad para ello y defendiendo la promesa de que cualquier transferencia sería a personas elegidas por la comunidad.

Entonces, la persona encargada de la administración de la cuenta de Gitea, responde negando dicha promesa, a lo que Loïc da indicaciones de lo establecido en la propia documentación.

## Opinión

Sinceramente, veo muy triste cómo algunas personas intentan defender acciones indefendibles como legitimar algo que está escrito como lo contrario, y no es que Loïc actuase de forma malintencionada o realizase ataques personales o similar.

Simplemente, dio el punto de vista de alguien que lleva observando el proyecto desde fuera algún tiempo y que, además, tiene conocimientos de cómo funciona.

También es triste ver cómo un proyecto comunitario podría estar empezando a ver su fin por las malas decisiones tomadas o hipocresía del equipo de mantenimiento, si aplica.

## Anexo

* Imagen de la conversación inicial: ![Conversación inicial entre Loïc Dachary y la persona encargada de la administración de la cuenta oficial de Gitea en el Fediverso](/resources/images/2022-10-27-gitea_es_ahora_propiedad_de_la_compania_gitea_ltd/1.webp "Imagen de la conversación inicial")

---

**Redacción:** [EchedeyLR](/redaction.html#EchedeyLR)