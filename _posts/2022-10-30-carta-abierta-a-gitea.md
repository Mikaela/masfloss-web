---
layout: post
lang: es
title: "Carta abierta a Gitea"
date: 2022-10-30 13:13:17 +0000
categories: news
---

Hace unos días [se hacía eco la noticia del cambio de propiedad de Gitea](https://masfloss.net/news/2022/10/27/gitea_es_ahora_propiedad_de_la_compania_gitea_ltd.html) a una entidad lucrativa y, con ello, la migración de la propiedad de las marcas y dominios asociados á la misma.

Ya llegando un poco por mi parte, casi de forma inmediata, se [redactó una carta abierta](https://gitea-open-letter.coding.social/) denunciando el cambio de propiedad sin un proceso elegido por la comunidad.

## Puntos abordados

La carta solicita los siguientes cambios a fin de asegurar la continuidad del proyecto y restaurar la confianza perdida:
* La creación de una entidad no-lucrativa cuya propiedad pertenezca a la comunidad de Gitea.
* La transferencia de marcas y dominios asociados con Gitea a dicha entidad.
* El nombre de la entidad lucrativa será modificado para evitar cualquier confusión con la entidad no-lucrativa.

## Enfoque de la entidad no-lucrativa

Asimismo, la idea detrás de dicha entidad no-lucrativa es abordar las siguientes mejoras:
* La implementación de un proceso de elección intuitivo y justo.
* La descripción de las formas en las que realizarán las decisiones democráticas.
* El desarrollo lugares accesibles donde encontrar toda la información relevante.
* El establecimiento de una [DoOcracia](https://communitywiki.org/wiki/DoOcracy) que funcione y continúe mejorándola.

## Consulta

Tras esto, ¿qué opináis de los cambios?.

¿Creéis que debería añadirse algo más?. O, por otro lado, ¿veis este proceso justo?.

---

**Redacción:** [EchedeyLR](/redaction.html#EchedeyLR)
