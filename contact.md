---
layout: page
lang: es
title: Contacto
---

## Canales
Actualmente, disponemos de un canal general disponible en distintas plataformas y enlazados entre las mismas. 

Las plataformas donde están disponibles son las siguientes:

* Matrix: [#general:masfloss.net](https://matrix.to/#/#general:masfloss.net)
* IRC: [#general:irc.masfloss.net](https://irc.masfloss.net/?channel=#general)

El primer canal tiene la temática del Software Libre y de Código Abierto, así como discusiones relacionadas con el blog y las publicaciones.

Por otro lado, el segundo canal sirve para aquellas conversaciones que se salgan del tema de la sala principal o que mantengan una muy débil relación.

Como nota adicional, se ha de destacar que no se pueden enviar ficheros de un lado a otro, siendo excepción todo lo que se envíe desde Matrix porque quedará disponible como enlace web al fichero hospedado en el propio servidor de Matrix.

## Directo
Una forma directa de ponerse en contacto con +FLOSS es enviar un mensaje por correo electrónico a la dirección `masfloss [at] riseup [dot] net`.

Dado que esta dirección es de uso principal, puede ser accedida por el equipo de redacción y, por ello, recomendamos que cualquier persona se abstenga de enviar información personal y/o sensible.

No hace falta mencionar que el SPAM y otros tipos de «correo basura» quedan fuera de lo permitido para enviar a esta dirección.
