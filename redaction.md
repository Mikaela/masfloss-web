---
layout: page
lang: es
title: Redacción
---

## EchedeyLR
Conocí el software libre a través de GNU/Linux a la edad de 14 años cuando, en secundaria, una profesora de Tecnologías mencionó “Linux” como otro sistema operativo, a lo que decidí buscar información. 

Tras un tiempo en ello, encontré información sobre el proyecto GNU y solo bastó leer la filosofía en su sitio web oficial para decidir que quería apoyarla. 

Desde el mismo año hasta el siguiente, realicé un cambio casi directo instalando SliTaz y luego Lubuntu.

Actualmente, uso Devuan con w9wm (rofi, dunst y lxpolkit) en casa y en mi portátil personal.

Mantengo la infraestructura ligada a masfloss.net, hispanilandia.net y subdominios de ambos.

Se me puede contactar por los siguientes medios:
* Matrix: `@echedeylr:masfloss.net`
* Delta Chat: `echedeylr@hispanilandia.net`
* Correo electrónico: `elr@disroot.org`

**Nota**: si me contactas por correo electrónico o un medio compatible, asegúrate de usar [mi clave GPG pública](https://keys.openpgp.org/pks/lookup?op=get&options=mr&search=elr@disroot.org) y enviarme la tuya adjunta para firmar y/o cifrar los mensajes.
